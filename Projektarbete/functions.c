#include "functions.h"

//kallar p� huvudmenyn som programmet utg�r fr�n
int huvudmeny(int boxArr[maxArr][maxArr], int antalSiffror)
{
	int guessArr[maxArr][maxArr];
	printVal();
	switch (getTal())
	{
	case 1:
		getBoxArr(boxArr, antalSiffror);
		printBoxArr(boxArr, antalSiffror);
		checkBox(boxArr, antalSiffror);
		getchar();
		if (checkBox(boxArr, antalSiffror) == 1)
		{
			int choice = 0;
			printf("vill du spara din kvadrat? 1 f�r ja, och annan siffra f�r nej.");
			scanf_s("%d", &choice);
			getchar();
			switch (choice)
			{
			case 1:
				saveBox(boxArr, antalSiffror);
				break;
			default:
				break;
			}
		}
		break;
	case 2:
		readBox(boxArr, antalSiffror);
		printBoxArr(boxArr, antalSiffror);
		checkBox(boxArr, antalSiffror);
		getchar();
		break;
	case 3:
		readBox(boxArr, antalSiffror);
		removeRandArray(boxArr, guessArr, antalSiffror);
		printBoxArr(guessArr, antalSiffror);
		guessBox(boxArr, guessArr, antalSiffror);
		printBoxArr(guessArr, antalSiffror);
		checkBox(guessArr, antalSiffror);
		getchar();
		break;
	default:
		break;
	}
}


//samlar alla funktioner som kollar om det �r en magicbox som �r inl�st.
int checkBox(int boxArr[maxArr][maxArr], int antalSiffror)
{
	int i = checkRad(boxArr, antalSiffror);
	int j = checkKolumn(boxArr, antalSiffror);
	int k = checkTal(boxArr, antalSiffror);
	int l = checkDiagonalLR(boxArr, antalSiffror);
	int m = checkDiagonalRL(boxArr, antalSiffror);
	if (i == 1 && j == 1 && k == 1 && l == 1 && m == 1)
	{
		printf("snygg l�da!");
		return 1;
	}
	else
	{
		printf("n�tt var fel!");
		return 0;
	}

}

//kollar summan av varje rad, �r summan 30 s� �r det godk�nnt.
int checkRad(int boxArr[maxArr][maxArr], int antalSiffror)
{
	int radSum = 0;
	for (int j = 0; j < antalSiffror; j++)
	{
		for (int i = 0; i < antalSiffror; i++)
		{
			radSum += boxArr[j][i];
		}

		if (radSum != 30)
		{
			printf("du har en felaktig rad %x\n", j);
			return 0;
		}
		radSum = 0;
	}
	return 1;
}

//kollar summan av varje kolumn, �r summan 30 s� �r det godk�nnt.
int checkKolumn(int boxArr[maxArr][maxArr], int antalSiffror)
{
	int kolumnSum = 0;
	for (int j = 0; j < antalSiffror; j++)
	{
		for (int i = 0; i < antalSiffror; i++)
		{
			kolumnSum += boxArr[i][j];
		}

		if (kolumnSum != 30)
		{
			printf("du har en felaktig kolumn %x\n", j);
			return 0;
		}
		kolumnSum = 0;
	}
	return 1;
}

//kollar summan av diagonalen fr�n �vre v�nstra h�rnet till nedre h�rgra. �r summan 30 s� �r det godk�nnt.
int checkDiagonalLR(int boxArr[maxArr][maxArr], int antalSiffror)
{
	int diagSum = 0;
	for (int i = 0, j = 0; j < antalSiffror || i < antalSiffror; i++, j++)
	{
		diagSum += boxArr[i][j];
	}
	if (diagSum != 30)
	{
		printf("din diagonal fr�n v�nster topp till h�ger bott �r fel\n");
		return 0;
	}
	return 1;
}

//kollar summan av diagonalen fr�n �vre h�gra h�rnet till nedre v�nstra. �r summan 30 s� �r det godk�nnt.
int checkDiagonalRL(int boxArr[maxArr][maxArr], int antalSiffror)
{
	int diagSum = 0;
	for (int i = 0, j = 3; j >= 0 || i < antalSiffror; i++, j--)
	{
		diagSum += boxArr[i][j];
	}
	if (diagSum != 30)
	{
		printf("din diagonal fr�n h�ger topp till v�nster bott �r fel\n");
		return 0;
	}
	return 1;
}

// kollar s� inget tal f�rekommer mer �n en g�ng.
int checkTal(int boxArr[maxArr][maxArr], int antalSiffror)
{
	int i = 0, j = 0, temprad = 0, tempkolumn = 0;
	for (temprad = 0; temprad <= antalSiffror && tempkolumn < antalSiffror; tempkolumn++)
	{

		if (tempkolumn == 4)
		{
			tempkolumn = 0;
			temprad++;
		}
		for (i = temprad; i < antalSiffror; i++)
		{
			for (j = 0; j < antalSiffror; j++)
			{
				if (tempkolumn == j && temprad == i)
					j++;
				if (boxArr[temprad][tempkolumn] == boxArr[i][j])
				{
					printf("det finns 2 lika tal\n");
					return 0;
				}
			}
		}
	}
	return 1;
}


//skriv ut talen fr�n arrayen
void printBoxArr(int boxArr[maxArr][maxArr], int antalSiffror)
{
	for (int i = 0; i < antalSiffror; i++)
	{
		for (int j = 0; j < antalSiffror; j++)
		{
			int siffra = boxArr[i][j];
			printf("%x \t", siffra);
		}
		printf("\n");
	}

}


// tar bort slumpm�ssiga tal, antal beroende p� vad anv�ndaren v�ljer, samma tal kan ej "tas bort" igen.
void removeRandArray(int boxArr[maxArr][maxArr], int guessArr[maxArr][maxArr], int antalsiffror)
{
	int antal = 0, removeR[8], removeC[8];
	readBox(guessArr, antalsiffror);
	printf("hur m�nga vill du gissa? v�lj mellan 1 och 8\n");
	scanf_s("%d", &antal);
	getchar();
	printf("\n");
	for (int i = 0; i < antal; i++)
	{
		srand((unsigned)time(NULL));
		removeR[i] = rand() % 4;
		removeC[i] = rand() % 4;
		for (int j = 0; j < i; j++)
			while (1)
			{
				if ((removeR[j] == removeR[i]) && (removeC[j] == removeC[i]))
				{
					removeR[i] = rand() % 4;
					removeC[i] = rand() % 4;
				}
				else
				{
					break;
				}
			}
		printf("%x\t", removeR[i]);
		printf("%x", removeC[i]);
		printf("\n");
	}
	printf("\n\n\n");
	for (int i = 0; i < antal; i++)
	{
		int r = removeR[i], c = removeC[i];
		guessArr[r][c] = 255;
	}


}


//kollar efter borttagna tal med en loop, n�r den hittar ett borttaget tal s� kallar den p� guessNumber.
void guessBox(int boxArr[maxArr][maxArr], int guessArr[maxArr][maxArr], int antalSiffror)
{
	for (int r = 0; r < antalSiffror; r++)
	{
		for (int c = 0; c < antalSiffror; c++)
		{
			if (guessArr[r][c] > 15)
				guessNumber(boxArr, guessArr, antalSiffror, r, c);
		}
	}
}

//anv�ndaren f�r gissa tal tills den har r�tt. ett tal �t g�ngen.
void guessNumber(int boxArr[maxArr][maxArr], int guessArr[maxArr][maxArr], int antalSiffror, int r, int c)
{
	int val;
	system("cls");
	printBoxArr(guessArr, antalSiffror);
	printf("gissa nummret som ska vara p� plats %d %d, det ska vara mellan 0 och 15\n", r, c);
	scanf("%d", &val);
	getchar();
	while (val > 16 && val < 0)
	{
		printf("felaktigt nummer. Gissa igen, denna g�ngen mellan 1 och 15!\n");
		scanf("%d", &val);
		getchar();
	}
	guessArr[r][c] = val;
	while (guessArr[r][c] != boxArr[r][c])
	{
		printf("fel nummer, gissa igen!\n");
		scanf("%d", &guessArr[r][c]);
		getchar();
	}
	printf("r�tt! bra gissat!\n");
	getchar();
}

//sparar en magicBox p� f�rsta raden i en textfil.
void saveBox(int boxArr[maxArr][maxArr], int antalSiffror)
{
	FILE *writeBox;
	writeBox = fopen("save.txt", "w");
	for (int i = 0; i < antalSiffror; ++i)
	{
		for (int j = 0; j < antalSiffror; j++)
		{
			int siffra = boxArr[i][j];
			fprintf(writeBox, "%x \t", siffra);
		}
	}
	fclose(writeBox);
}

//l�ser in f�rsta raden i en textfil.
void readBox(int boxArr[maxArr][maxArr], int antalSiffror)
{
	FILE *readBox;
	int buffer;
	readBox = fopen("save.txt", "r");
	int r = 0, c = 0;
	while (fscanf(readBox, "%x", &buffer) != EOF)
	{
		if (buffer != ' ')
		{
			boxArr[r][c] = buffer;
			c++;
			if (c >= antalSiffror)
			{
				r++;
				c = 0;
			}
		}

	}

	fclose(readBox);
}

//anv�ndaren f�r skriva in alla tal i en magicbox.
void getBoxArr(int boxArr[maxArr][maxArr], int antalSiffror)
{
	for (int i = 0; i < antalSiffror; i++)
	{
		for (int j = 0, nummer = 0; j < antalSiffror; j++)
		{
			printf("\nskriv det tal du vill l�gga p� rad %d kolumn %d\n", i, j);
			nummer = getTal();
			if (nummer < 0 || nummer >15)
			{
				printf("skriv ett tal mellan 0 och 15");
				nummer = getTal();
			}
			boxArr[i][j] = nummer;
			system("cls");
		}
	}

}

