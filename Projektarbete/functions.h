#pragma once
#include <stdio.h>
#include <stdlib.h>
#pragma warning (disable : 4996)

#define maxArr 200
const char EOL = '\n';


int huvudmeny(int[maxArr][maxArr], int);//kallar p� huvudmenyn som programmet utg�r fr�n


int checkBox(int[maxArr][maxArr], int);//samlar alla funktioner som kollar om det �r en magicbox som �r inl�st.

int checkRad(int[maxArr][maxArr], int);//kollar summan av varje rad, �r summan 30 s� �r det godk�nnt.
int checkKolumn(int[maxArr][maxArr], int);//kollar summan av varje kolumn, �r summan 30 s� �r det godk�nnt.
int checkDiagonalLR(int[maxArr][maxArr], int);//kollar summan av diagonalen fr�n �vre v�nstra h�rnet till nedre h�rgra. �r summan 30 s� �r det godk�nnt.
int checkDiagonalRL(int[maxArr][maxArr], int);//kollar summan av diagonalen fr�n �vre h�gra h�rnet till nedre v�nstra. �r summan 30 s� �r det godk�nnt.
int checkTal(int[maxArr][maxArr], int);// kollar s� inget tal f�rekommer mer �n en g�ng.


void printBoxArr(int[maxArr][maxArr], int);//skriv ut talen fr�n arrayen


void removeRandArray(int[maxArr][maxArr], int[maxArr][maxArr], int);// tar bort slumpm�ssiga tal, antal beroende p� vad anv�ndaren v�ljer, samma tal kan ej "tas bort" igen.

void guessBox(int[maxArr][maxArr], int[maxArr][maxArr], int);//kollar efter borttagna tal med en loop, n�r den hittar ett borttaget tal s� kallar den p� guessNumber.

void guessNumber(int[maxArr][maxArr], int[maxArr][maxArr], int, int, int);//anv�ndaren f�r gissa tal tills den har r�tt. ett tal �t g�ngen.


void saveBox(int[maxArr][maxArr], int);//sparar en magicBox p� f�rsta raden i en textfil.
void readBox(int[maxArr][maxArr], int);//l�ser in f�rsta raden i en textfil.


void getBoxArr(int[maxArr][maxArr], int);//anv�ndaren f�r skriva in alla tal i en magicbox.